import React, { Component } from "react";
import { Text, View, Image, StyleSheet } from 'react-native';

class FaceButton extends Component {
    render(){
        return(
            <View style={[styles.containerBut, this.props.style]}>
                <View style={styles.containerIcon}>
                    <Image style={styles.iconn} source={this.props.source}/>
                </View>
                <Text style={styles.textSt}>{this.props.title}</Text>
                <View style={{ flex: 1, flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
                    <Image style={styles.bbbVector} source={require("../assets/images/Vectorbbb.png")} />
                    <Image style={{ width: 5.44, height: 12 }} source={require("../assets/images/Vectorvvv.png")} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerBut: { 
        padding: 6, 
        flex: 1, 
        marginRight: 25, 
        marginLeft: 25, 
        flexDirection: "row", 
        justifyContent: "center", 
        alignItems: "center", 
        borderRadius: 100 
    },
    containerIcon: {
        marginLeft: 20, 
        right: 15, 
        width: 40, 
        height: 40, 
        backgroundColor: "#060B00", 
        borderRadius: 100
    },
    iconn: {
        left: "24%", 
        top: "18%", 
        width: 17, 
        height: 32
    }, 
    textSt: {
        marginRight: 5, 
        color: "#060B00", 
        fontFamily: "Gilroy-Light", 
        fontSize: 16, 
        lineHeight: 19, 
        fontWeight: "bold", 
        height: 19, 
        width: 195
    },
    bbbVector: {
        height: 3,
        borderStyle: "solid", 
        borderColor: "#060B00", 
        width: 14
    }
})

export default FaceButton