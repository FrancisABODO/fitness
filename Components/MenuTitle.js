import React, { Component } from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'

export default class MenuTitle extends Component {
    render() {
        return (
            <View style={styles.containTitleMenu}>
                <Image style={styles.titleIcon} source={require("../assets/images/Group4f.png")} />
                <Text style={styles.Title}>
                    {this.props.title}
                </Text>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    Title: {
        fontFamily: "Gilroy-ExtraBold",
        fontSize: 14,
        fontWeight: "bold",
        lineHeight: 17,
        height: 21,
        width: 160,
        color: "#060B00",
        marginLeft: 10,
        marginTop: 7
        // borderColor: "red", borderWidth: 3, borderStyle: "solid"
    },
    titleIcon: {
        width: 21.39,
        height: 21,
        marginTop: 13,
        marginLeft: 15,
        marginBottom: 13
    },
    containTitleMenu: {
        height: 48,
        width: 285,
        marginTop: 25,
        marginLeft: 20,
        marginRight: 20,
        backgroundColor: "#FFFFFF",
        borderWidth: 1,
        borderStyle: "solid",
        borderColor: "#060B00",
        borderRadius: 10,
        //flex: 1,
        flexDirection: "row",
        alignItems: "center"
    }
})