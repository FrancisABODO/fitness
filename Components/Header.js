import React from "react";

import { Image, StyleSheet } from 'react-native';

class Header extends React.Component{
    render() {
        return(            
          <Image
            style={styles.icon}
            source={require("../assets/images/Imageheader.svg")}
         />   
        );
    }
}

const styles = StyleSheet.create({
    icon: {
        //position: "absolute",
        top: 100,
        width: 376,
        height: 285,
        resizeMode: "contain"
      },
})

export default Header