import React, { Component } from "react";
import { Text, View, Image, StyleSheet } from 'react-native';

class EmailButton extends Component {
    render(){
        return(
            <View style={[styles.containerBut, this.props.style]}>
                <View style={{ marginLeft: 20, right: 15, width: 40, height: 40, backgroundColor: "#FFFFFF", borderRadius: 100 }}>
                    <Image style={{ left: "24%", top: "30%", width: 20, height: 16}} source={this.props.source}/>
                </View>
                <Text style={styles.textview}>{this.props.title}</Text>
                <View style={{ flex: 1, flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
                    <Image style={styles.tick} source={require("../assets/images/Vectorlll.png")} />
                    <Image style={{ width: 5.44, height: 12 }} source={require("../assets/images/Vectortick.png")} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerBut: { 
        padding: 6, 
        flex: 1, 
        marginRight: 25, 
        marginLeft: 25, 
        flexDirection: "row", 
        justifyContent: "center", 
        alignItems: "center", 
        borderRadius: 100 
    },
    tick: {
        height: 3,
        borderStyle: "solid", 
        borderColor: "#FFFFFF", 
        width: 14
    },
    textview: {
        marginRight: 5, 
        color: "#FFFFFF", 
        fontFamily: "Gilroy-Light", 
        fontSize: 16, 
        lineHeight: 19, 
        fontWeight: "bold", 
        height: 19, 
        width: 195
    }
})

export default EmailButton