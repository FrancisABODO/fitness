import React from 'react';

import { Text, View, StyleSheet, ActivityIndicator } from 'react-native';

import * as Font  from 'expo-font';
import EmailButton from './EmailButton';
import FaceButton from './FaceButton';

class Body extends React.Component{
    constructor(){
        super();
        this.state = {
            fontLoaded:false
        }
    }

    async componentDidMount(){
        await Font.loadAsync({
            'Gilroy-ExtraBold': require('../assets/fonts/Gilroy-ExtraBold.otf'),
            'Gilroy-Light': require('../assets/fonts/Gilroy-Light.otf')
        });

        this.setState({ fontLoaded: true });
    }
    render() {
        return (
            <View style={styles.card}>
                {this.state.fontLoaded ? (
                <Text style={styles.text}>                     
                    Comment Souhaitez-Vous Vous Inscrire ?
                </Text> ) : ( <ActivityIndicator size="large" /> )}
                <View>
                    <EmailButton style={{backgroundColor: "#1557A5", marginTop: 65, color: "#1557A5"}} source={require("../assets/images/Vector.png")} title={"Continuer Avec l'email"} />
                    <FaceButton style={{backgroundColor: "white", marginTop: 45}} source={require("../assets/images/Vectorfcbk.png")} title={"Continuer Avec Facebook"} />
                </View>
            </View> 
        );
    }
}

const styles = StyleSheet.create({
    text: {
        marginLeft: 35, 
        marginRight: 35, 
        marginTop: 35, 
        textAlign: "center", 
        fontWeight: "bold", 
        lineHeight: 36, 
        color: "white", 
        fontFamily: "Gilroy-Light", 
        fontSize: 24,    
    },
    card: {
        top: 150,
        width: 375,
        height: 375,
        backgroundColor: "#060B00",
        borderTopLeftRadius: 35,
        borderTopRightRadius: 35,
    },
})

export default Body