import React, { Component } from 'react'

import { View, Image, StyleSheet, Text, ActivityIndicator } from 'react-native'
//import { LinearGradient } from 'expo-linear-gradient'
import * as Font  from 'expo-font';

export class HeadCSearch extends Component {
    constructor(){
        super();
        this.state = {
            fontLoaded:false
        }
    }

    async componentDidMount(){
        await Font.loadAsync({
            'Gilroy-ExtraBold': require('../assets/fonts/Gilroy-ExtraBold.otf'),
            'Gilroy-Light': require('../assets/fonts/Gilroy-Light.otf')
        });

        this.setState({ fontLoaded: true });
    }
    render() {
        return (
            <View style={{ 
                flex: 1, 
                flexDirection: "row", 
            //    borderColor: "red", borderWidth: 3, borderStyle: "solid" 
            }}>
                {/* <LinearGradient colors={[ "#FFFFFF", "#060B00"]} style={styles.lineraGradient}> */}
                    <Image style={styles.coachI} source={this.props.source2} />            
                {/* </LinearGradient> */}
                {this.state.fontLoaded ? (
                <Text style={styles.nameC}>{this.props.coach}</Text>) : ( <ActivityIndicator size="large" /> )}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    lineraGradient: {
        width: 50, 
        height: 50,
        top: 75,
        left: 25,
    },
    nameC: {
        color: "#FFFFFF",
        fontFamily: "Gilroy-Light",
        fontSize: 24,
        lineHeight: 29,
        fontWeight: "bold",
        //borderColor: "blue", borderWidth: 3, borderStyle: "solid",
        //width: 141,
        height: 29,
        marginLeft: 55,
        marginTop: 85
    },
    coachI: {
        width: 42.5,
        height: 42.5,
        top: 78.75,
        left: 28.75,
        backgroundColor: "#C4C4C4",
        //borderColor: "green", borderWidth: 3, borderStyle: "solid"
    }
})

export default HeadCSearch
