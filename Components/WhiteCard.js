import React, { Component } from 'react'
import { View, StyleSheet, Text, Image } from 'react-native'
import MenuTitle from './MenuTitle'

export class WhiteCard extends Component {
    render() {
        return (
            <View style={{
                borderTopLeftRadius: 35,
                borderTopRightRadius: 35,
                backgroundColor: "#FFFFFF",
                height: 652,
                //top: 100,
                left: 0
            }}>
                <Text style={[styles.partenaire, this.props.style]}>
                    {this.props.partenaire}
                </Text>
                <Text style={[styles.sport, this.props.style]}>
                    {this.props.sport}                    
                </Text>
                <View style={styles.boxInput}>
                    <MenuTitle title={"Fitness"} />
                </View>                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    boxInput: {
        backgroundColor: "#F4F8EF",
        borderRadius: 25,
        marginTop: 35,
        marginLeft: 25,
        marginRight: 25,
        height: 390
    },
    partenaire: {
        fontFamily: "Gilroy-ExtraBold",
        fontSize: 24,
        fontWeight: "bold",
        lineHeight: 29,
        marginTop: 35,
        textAlign: "center",
    },
    sport: {
        fontFamily: "Gilroy-Light",
        fontStyle: "normal",
        fontWeight: 500,
        fontSize: 14,
        lineHeight: 16,
        marginTop: 10
    },
    box: {

    }
})

export default WhiteCard
