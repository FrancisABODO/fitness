import React from 'react'
import { View, StyleSheet } from 'react-native'
import Body from '../Components/Body'
import Header from '../Components/Header'

//import { ReactComponent as HeaderIcon } from '../assets/images/Imageheader.svg'

const Register = () => {
    return (
      <View style={{flex: 1}}>
        <Header />
        <Body />               
      </View>
    )
}
  

const styles = StyleSheet.create({
  secondButton: {
    color: "black", 
    backgroundColor: "white"
  },
  
  containerCard: {
    flex: 1,
    justifyContent: "center",
  },
  
})

export default Register
