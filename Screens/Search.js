import React from 'react';

import { View, StyleSheet } from 'react-native';
import HeadCSearch from '../Components/HeadCSearch';
import WhiteCard from '../Components/WhiteCard';

const Search = () => {
    return(
        <View style={styles.containerS}>
            <HeadCSearch coach={"John Wilkins"} source2={require("../assets/images/Ellipse2iCoach.png")} />
            <WhiteCard style={styles.whiteText} sport={"Et Pratiquer Le Sport De Votre Choix"} partenaire={"Trouvez Votre Partenaire"} />
        </View>

    );
}

const styles = StyleSheet.create({
    containerS: {
        backgroundColor: "#060B00",
        width: "100%",
        height: "100%"
    },
    whiteText: {
        fontStyle: "normal",
        fontWeight: "bold",
        textAlign: "center",
        color: "#060B00",
    }
})

export default Search